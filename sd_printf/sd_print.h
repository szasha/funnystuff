#ifndef SD_PRINT_INCLUDED
#define SD_PRINT_INCLUDED

#include <stdio.h>

/*
 * printf ...
 * Prints passed var list of arguments to stdout according to passed format spec,
 * returns number of printed characters.
 */
int sd_printf(const char *format, ...);

/*
 * fprintf ...
 * Prints passed var list of arguments to passed file according to passed format spec,
 * returns number of printed characters.
 */
int sd_fprintf(FILE *out, const char *format, ...);

/*
 * Print passed int to passed file.
 * return number of printed characters.
 */
int sd_print_int(FILE *out, int num);
#endif
