#include <stdio.h>
#include "sd_print.h"

int main(int argc, char *argv[]){
    int printed = sd_printf("hello world %% %i %i\n", 23, -101);
    sd_printf("We printed %i characters to stdout\n", printed);

    FILE *f = fopen("test.txt", "wb");
    printed = sd_fprintf(f, "hello world %% %i %i\n", 23, -101);
    sd_printf("And %i characters to test.txt\n", printed);
    fclose(f);

    return 0;
}

