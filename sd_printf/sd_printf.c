#include <stdio.h>
#include <stdarg.h>
#include "sd_print.h"


// Lets invent a datatype!
typedef int bool;
#define true 1
#define false 0


static int _printf(FILE *out, const char *format, va_list args){
    int  i = 0;     // Index to access the chars in the format string
    int  l = 0;     // To be returned, the number of printed characters
    bool f = false; // True if encountered a '%' and expecting an argument spec in the next char in the format string

    while(1){
        char c = format[i];
        i++;

        if(c == '\0'){ // EOL
            break;
        }

        if(f){
            // Previous char was a '%' (and without a '%' bevore it)
            // format current varg acording to spec in current char
            f = false;
            if(c == '%'){
                // Oh, its not a spec, just an escaped '%'.
                // Print '%'
                putc('%', out);
                l++;
            }

            else if(c == 'i'){
                // Current varg is specified as an int
                // Print the int
                l = l + sd_print_int(out, va_arg(args, int));
            }

            else{
                // Unknown/not-yet-implemented spec
                // XXX XXX Continue development here
                // For now just print the previous '%' and the char
                putc('%', out);
                putc(c, out);
                l = l + 2;
            }

            continue;
        }

        if(c == '%'){
            // Next char will be a format spec
            f = true;
            continue;
        }

        // Nothing special, just print the char
        putc(c, out);
        l++;

    }

    return l; // Return number of printed characters
}


int sd_printf(const char *format, ...){
    va_list args;                             // Holds the variable arguments list
    va_start(args, format);                   // Initialize the variable arguments list
    FILE *out = stdout;                       // sd_printf implicitly prints to stdout
    int printed = _printf(out, format, args); // Call our actual _printf
    va_end(args);                             // Release the variable arguments list
    return printed;
}


int sd_fprintf(FILE *out, const char *format, ...){
    va_list args;                             // Holds the variable arguments list
    va_start(args, format);                   // Initialize the variable arguments list
    int printed = _printf(out, format, args); // Call our actual _printf
    va_end(args);                             // Release the variable arguments list
    return printed;
}
