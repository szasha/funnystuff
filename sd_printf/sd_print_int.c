#include <stdio.h>
#include "sd_print.h"


int sd_print_int(FILE *fout, int num){
    int printed = 0;
    if (num < 0){
        putc('-', fout);
        printed++;
        num = -num;
    }
    if (num > 9){
        printed = printed + sd_print_int(fout, num/10);
    }
    putc('0' + (num%10), fout);
    printed++;
    return printed;
}
